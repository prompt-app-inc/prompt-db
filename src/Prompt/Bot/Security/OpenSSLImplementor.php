<?php
/**
 *      OpenSSLImplementor.abstract.php
 *      
 *      Part of the Prompt bespoke framework. Copyright PROMPT Inc 2014 - 2015.
 *      This file may not be replicated in whole or in part without prior
 *      permission of PROMPT Inc. All enquiries james@promptapp.io
 *      @author James Cundle james@promptapp.io
 *      @copyright Copyright PROMPT Inc 2014 - 2015.
 *
 *      @package  PROMPTCommon
 */

namespace Prompt\Bot\Security;

use Prompt\MySQL;


/**
 *      This class provides template functionality for OpenSSL classes.
 *
 *      Example:
 *      <code>
 *      </code>
*/
abstract class OpenSSLImplementor extends MySQL\MYSQLTableAccessor {

        protected $encrypt;
        
        protected $defaultpublickey = "";
        

        public function __construct($params=array()) {	
                $rootdir = (isset($_SERVER['DOCUMENT_ROOT'])) ? realpath(sprintf('%s/../',$_SERVER['DOCUMENT_ROOT'])) : NULL;                           
                $params = array_merge(array('rootdir'=>$rootdir,'privatekeyfile'=>'app/resources/keys/default.key', 'publickeyfile'=>'app/resources/keys/default.pem'), $params);
                $this->encrypt = new OpenSSLEncryption();
                if($params['rootdir']) {
                    $this->loadPublicKey(sprintf('%s/%s', $params['rootdir'], $params['publickeyfile']));
                    $this->loadPrivateKey(sprintf('%s/%s', $params['rootdir'], $params['privatekeyfile']));
                } else {
                    error_log("Security\OpenSSLImplementor() - parameter 'rootdir' not supplied. Can not autoload keys.");
                }
                
		parent::__construct($params);
	}
        
        public function loadPublicKey($filename) {
            $str = file_get_contents($filename);
            if($str) {
                $this->setPublicKey($str);
            }
        }
        
        public function loadPrivateKey($filename) {
            $str = file_get_contents($filename);
            if($str) {
                $this->setPrivateKey($str);
            }
        }
        
        public function setPrivateKey($key) {
            $this->encrypt->setPrivateKey($key);		
        }

        public function setPublicKey($key) {
                $this->encrypt->setPublicKey($key);		
        }

        public function setPassphrase($passphrase) {
                $this->encrypt->setPassphrase($passphrase);		
        }        
        
        public function isPrivateKeyLoaded() {
            return $this->encrypt->getPrivateKey()!="";
        }

        public function encrypt($v) {
            return $this->encrypt->encrypt($v);
        }

        public function decrypt($v) {
            return $this->encrypt->decrypt($v);
        }
        
      public function setPrivateKeyCookie($str) {
          $b64str = (base64_decode($str, true)) ? $str : base64_encode($str);
          setcookie("ccpk",$b64str,NULL,"/");
          $_COOKIE['ccpk']=$b64str;
      }
      
      public function getPrivateKeyCookie() {
          return (isset($_COOKIE['ccpk'])) ? base64_decode($_COOKIE['ccpk']) : NULL;
      }      
        
      public function initPrivateKey() {
          $this->setPrivateKey($this->getPrivateKeyCookie());
      }
      
      
}
