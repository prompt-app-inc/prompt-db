<?php

/**
 *      Users.class.php
 *      
 *      Part of the Prompt bespoke framework. Copyright PROMPT Inc 2014 - 2015.
 *      This file may not be replicated in whole or in part without prior
 *      permission of PROMPT Inc. All enquiries james@promptapp.io
 *      @author James Cundle james@promptapp.io
 *      @copyright Copyright PROMPT Inc 2014 - 2015.
 *
 *      @package  PROMPTCommon
 */

namespace Prompt\Bot\User;

use Prompt\MySQL;


/**
 *      This class provides a complete set of functionality for Users including Google+ Integration.
 *
 *      Example:
 *      <code>
 *      </code>
 */
class User extends MySQL\MYSQLTableAccessor {
    # $table = MYSQL Table, $serialkey = Table Serial Key Column(s), $fields = List of fields restricted for update

    protected $table = "users";
    protected $serialkey = array("userid");
    protected $permittedAjaxFields = array(
                                            'bot' => array(),
    );
    protected $lookupdata = array();
    protected $usercredentials;
    
    public function __construct($params = array()) {
        parent::__construct($params);
    }

    public function getUserByUUID($uuid) {
        if ($uuid) {
            $sql = sprintf("uuid = '%s' AND deleted = 0", $this->MYSQLEscapeString($uuid));
            $this->load(NULL, $sql);
        }
        return $this->isLoaded();
    }

    public function save() {
        return parent::save();
    }
    
    public function delete($userid = NULL, $wheresql = NULL) {
        $userid = ($userid) ? $userid : $this->getUserID();
        if ($userid && $this->load($userid)) {
            $this->setEmail(sprintf('DELETED-%s-%s', rand(), $this->getEmail()));
            $this->setCellphone(sprintf('DELETED-%s-%s', rand(), $this->getCellphone()));
            $this->setEnabled(0);
            $this->setDeleted(1);
            $this->save();
        }
    }    
    
    public function getUsercredentials($typecode=NULL) {
        if($typecode && (!$this->usercredentials || $this->usercredentials->getTypeCode() != $typecode)) {
            $this->usercredentials = NULL;
            if($this->getUserID()) {
                $this->usercredentials = new UserCredentials();
                $this->usercredentials->getUserCredentialByType($this->getUserID(), $typecode);
            }
        }
        return $this->usercredentials;
    }    
    
}
