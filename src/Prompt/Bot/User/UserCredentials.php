<?php

/**
 *      CardDetails.class.php
 *      
 *      Part of the Prompt bespoke framework. Copyright PROMPT Inc 2014 - 2015.
 *      This file may not be replicated in whole or in part without prior
 *      permission of PROMPT Inc. All enquiries james@buywithfetch.com
 *      @author James Cundle james@buywithfetch.com
 *      @copyright Copyright PROMPT Inc 2014 - 2015.
 *
 *      @package  PROMPTCommon
 */

namespace Prompt\Bot\User;

use Prompt\MySQL;
use Prompt\Bot\Security;
use Prompt\Core;


/**
 *      This class provides functions for Card Details.
 *      A set of utility functions for handling Credit Cards.
 *
 *      Example:
 *      <code>
 *      </code>
 */
class UserCredentials extends Security\OpenSSLImplementor {
    # $table = MYSQL Table, $serialkey = Table Serial Key Column(s), $fields = List of fields restricted for update

    protected $table = "usercredentials";
    protected $serialkey = array("usercredentialid");
    protected $lookupdata = array();
    protected $vfcc;
    protected $permittedAjaxFields = array();

    public function __construct($params = array()) {
        $params = array_merge(array(), $params);
        parent::__construct($params);
    }
    
    public function getUsername()
    {
        $username = $this->decrypt(parent::getUsername());        
        return $username;
    }    
    
    public function setUsername($username)
    {
        parent::setUsername($this->encrypt($username));
        return $username;
    }    

    public function getUserpasswd()
    {
        $userpasswd = $this->decrypt(parent::getUserpasswd());        
        return $userpasswd;
    }    
    
    public function setUserpasswd($userpasswd)
    {
        parent::setUserpasswd($this->encrypt($userpasswd));
        return $userpasswd;
    } 
        

    public function getUserCredentialByID($userid, $usercredentalid) {
        return $this->load(NULL, sprintf('usercredentalid = "%s" AND userid = "%s"', $this->MYSQLEscapeString($usercredentalid), $this->MYSQLEscapeString($userid)));
    }

    public function getUserCredentialByType($userid, $typecode) {
        return $this->load(NULL, sprintf('typecode = "%s" AND userid = "%s" AND deleted = "No"', $this->MYSQLEscapeString($typecode), $this->MYSQLEscapeString($userid)));
    }
    
    public function hasUserCredentialByType($userid, $typecode) {
        $this->initialize(true);
        $credentials = $this->getUserCredentialByType($userid, $typecode);
        return $this->isLoaded();
    }
    
    public function credentialTypeExists($userid, $typecode) {
        $sql = sprintf('SELECT COUNT(*) AS num FROM usercredentials WHERE typecode = "%s" AND userid = "%s" AND deleted = "No"', 
                        $this->MYSQLEscapeString($typecode), $this->MYSQLEscapeString($userid)
                        );

        $this->rs->query($sql);
        $row = $this->rs->fetchNext();
        return $row['num']>0;
    }    
    
    // Override hard-delete function
    public function delete($keyvalues = NULL, $wheresql = NULL) {
        $deleted = false;
        if ($this->load($keyvalues, $wheresql)) {
            $ok = true;
            $this->setDeleted('Yes');
            $result = $this->save();
            $deleted = $result['affectedrows'] > 0;
        }
        return $deleted;
    }

    public function deleteByUserID($userid) {
        if ($userid) {
            $sql = sprintf("UPDATE %s SET deleted = 'Yes' WHERE userid = '%s'", $this->rs->MYSQLEscapeString($this->table), $this->rs->MYSQLEscapeString($userid));
            $this->rs->execute($sql);
        }
    }

    public function save() {
        $ok = true;
        $return = false;

        if (!$this->getDeleted()) {
            $this->setDeleted('No');
        }
        $this->setLastupdated(date("Y-m-d H:i:s"));

        if ($ok) {
            $return = parent::save();
        }


        return $return;
    }

    public function getUserCredentials($userid, $params = array()) {

        $params = array_merge(array("start" => 0, "limit" => 50, "orderby" => "uc.usercredentialid", "filter" => "", "deleted" => "No"), $params);
        $sql = sprintf("SELECT uc.* FROM usercredentials uc WHERE userid = '%s'", $this->MYSQLEscapeString($userid));

        if (!$params['deleted'] || strtolower($params['deleted']) != 'yes') {
            $sql.=sprintf(" AND deleted = 'No'");
        }

        if ($params['filter']) {
            $sql.=sprintf(" AND %s", $params['filter']);
        }

        $sql.=sprintf(" ORDER BY %s", $this->MYSQLEscapeString($params['orderby']));

        if (isset($params['start']) && is_numeric($params['start']) && isset($params['limit']) && is_numeric($params['limit'])) {
            $sql.=sprintf(" LIMIT %s,%s", $this->MYSQLEscapeString($params['start']), $this->MYSQLEscapeString($params['limit']));
        }


        $this->rs->query($sql);
        return $this->rs->fetchAllRows();
    }

    public function countUserCredentials($userid, $params = array()) {
        $params = array_merge(array("filter" => "", "deleted" => "No"), $params);
        $sql = sprintf('SELECT COUNT(*) AS num FROM usercredentials WHERE userid = "%s" AND deleted = "No"', $this->MYSQLEscapeString($userid));
        
        if ($params['deleted']) {
            $sql.=sprintf(" AND deleted = '%s'", (strtolower($params['deleted']) == 'yes') ? 'Yes' : 'No' );
        }
        if ($params['filter']) {
            $sql.=sprintf(" AND %s", $params['filter']);
        }
        $this->rs->query($sql);
        $row = $this->rs->fetchNext();
        return $row['num'];
    }


}

?>