<?php

// Built to reduce number of DB connections per script instance

namespace Prompt\MySQL;

class MYSQLDBConnectionPool {
    private static $connections = array();
    
    public static function setConnection($hostname, $database, $username, $password, & $connection) {
        $key = self::makeKey($hostname, $database, $username, $password);
        self::$connections[$key] = $connection;
    }
    
    public static function getConnection($hostname, $database, $username, $password) {
        $key = self::makeKey($hostname, $database, $username, $password);
        return (isset(self::$connections[$key])) ? self::$connections[$key] : NULL;
    }
    
    public static function makeKey($hostname, $database, $username, $password) {
        return md5(sprintf("%s%s%s%s", $hostname, $database, $username, $password));
    }    

    public static function closeConnection($hostname, $database, $username, $password) {
        $key = self::makeKey($hostname, $database, $username, $password);
        return (isset(self::$connections[$key])) ? @self::$connections[$key]->close() : NULL;
    }
    
    public static function close() {
        foreach(self::$connections as $connection) {
            $connection->close();
        }
    }
    
}
