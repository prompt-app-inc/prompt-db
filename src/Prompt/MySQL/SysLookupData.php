<?php

namespace Prompt\MySQL;

class SysLookupData {
    
    protected $rs;
    
    public function __construct() {
        $this->rs = new \Prompt\MYSQLRecordset();
    }
    
    public function getSysLookupData($params=array()) {
        $params = array_merge(array('dbtable'=>NULL,'dbcolumn'=>NULL,'filter'=>NULL,'deleted'=>false,'lang'=>'en-us','beginswith'=>NULL,'codebeginswith'=>NULL,'orderby'=>'orderby'), $params);

        $sql=sprintf('SELECT lookupid,dbtable, dbcolumn, lookupcode,lookupvalue, orderby FROM syslookupdata WHERE 1=1');
        
        if($params['dbtable']) {
            $sql.=sprintf(' AND dbtable = "%s"',
                        $this->rs->MYSQLEscapeString($params['dbtable'])
                    );
        }
        
        if($params['dbcolumn']) {
            $sql.=sprintf(' AND dbcolumn = "%s"',
                        $this->rs->MYSQLEscapeString($params['dbcolumn'])
                    );
        }
        
        if($params['filter']) {
            $sql.=sprintf(' AND %s',$params['filter']);
        }
        
        if($params['codebeginswith']) {
            $sql.=sprintf(' AND lookupcode LIKE "%s%%"',$params['codebeginswith']);
        }        
        
        if($params['beginswith']) {
            $sql.=sprintf(' AND lookupvalue LIKE "%s%%"',$params['beginswith']);
        }        
        
        if(!$params['deleted']) {
            $sql.=sprintf(' AND deleted = "No"');            
        }
        
        if($params['lang']) {
            $sql.=sprintf(' AND lang = "%s"',$params['lang']);
        }
        
        $sql.=sprintf(' ORDER BY %s', $params['orderby']);

        $this->rs->query($sql);
        
        return $this->rs->fetchAllRows();
    }    
    
    public function getGroupedSysLookupData($params=array()) {
        $params = array_merge(array('dbtable'=>NULL,'dbcolumn'=>NULL,'filter'=>NULL), $params);  
        $retArr=array();
        foreach($this->getSysLookupData(array('dbtable'=>$params['dbtable'],'dbcolumn'=>$params['dbcolumn'])) as $row) {
            if(!isset($retArr[$row['dbtable']])) {
                $retArr[$row['dbtable']]=array();
            }
            if(!isset($retArr[$row['dbtable']][$row['dbcolumn']])) {
               $retArr[$row['dbtable']][$row['dbcolumn']]=array();
            }
            $retArr[$row['dbtable']][$row['dbcolumn']][] = array('lookupcode'=>$row['lookupcode'],'lookupvalue'=>$row['lookupvalue'],'orderby'=>$row['orderby']);
            ##$returndata['syslookup'] = array('')
        }
        return $retArr;
    }
    
    public function loadDataValue($dbtable,$dbcolumn,$lookupcode,$returnid=false) {
        $retval=NULL;
        $rows = $this->getSysLookupData(array('dbtable='>$dbtable,'dbcolumn'=>$dbcolumn,'filter'=>sprintf('lookupcode = "%s"',$this->rs->MYSQLEscapeString($lookupcode))));
        if(count($rows)>0) {
            $row = $rows[0];
            $retval=($returnid) ? $row['lookupid'] : $row['lookupvalue'];
        }
        return $retval;
    }
    
    public function validateDataValue($dbtable,$dbcolumn,$value) {
        $sql=sprintf("SELECT COUNT(*) AS num FROM syslookupdata WHERE dbtable = '%s' AND dbcolumn = '%s' AND lookupcode = '%s'", 
                    $this->rs->MYSQLEscapeString($dbtable),
                    $this->rs->MYSQLEscapeString($dbcolumn),
                    $this->rs->MYSQLEscapeString($value)                    
                );
        $this->rs->query($sql);
        $row=$this->rs->fetchNext();
        return $row['num']>0;
    }    
    
    public function insertDataValue($dbtable,$dbcolumn,$lookupcode, $lookupvalue, $orderby=NULL) {
        $id=$this->loadDataValue($dbtable,$dbcolumn,$lookupcode,true);
        if(!$id) {
            $sql=sprintf("INSERT INTO syslookupdata (dbtable,dbcolumn,lookupcode,lookupvalue,orderby) VALUES ('%s','%s','%s','%s',%s)",
                    $this->rs->MYSQLEscapeString($dbtable),
                    $this->rs->MYSQLEscapeString($dbcolumn),
                    $this->rs->MYSQLEscapeString($lookupcode),
                    $this->rs->MYSQLEscapeString($lookupvalue),
                    $this->rs->MYSQLEscapeString(($orderby) ? sprintf("'%s'", $orderby) : "NULL")
                    );
            $result = $this->rs->execute($sql);
            $id = (isset($result['insertid'])) ? $result['insertid'] : NULL;
            if($id) {
                $this->lookupdata[$column][$lookupcode]=$lookupvalue; 
            }
        }
        return $id;
    }    
    
}