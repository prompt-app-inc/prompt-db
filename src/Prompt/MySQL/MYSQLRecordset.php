<?php
/**
 *      MYSQLRecordset.class.php
*/
/**
 *		Provided and owned by Blue World Media as part of the bespoke framework
 *		utility functions. Provided under licence.
 *
 *      This class provides direct MYSQL Table access 
 *
 *
 *      Example:
 *      <code>
 *		$rs = new Prompt\MYSQLRecordset();
 *		$sql=sprintf("SELECT * FROM users WHERE lastupdated > '%s''", $rs->MYSQLEscapeString($lastupdated));
 *		$rs->query($sql);
 *		if($rs->getCount()>0) {
 *			while($row = $rs->fetchNext()) {
 *				echo sprintf("%s\n", $row['emai']);
 *			}
 *		}
 *	
 *		or
 *
 *		$sql=sprintf("SELECT * FROM users WHERE lastupdated > '%s''", $rs->MYSQLEscapeString($lastupdated));
 *		$rs->query($sql);
 *		$rows = $rs->fetchAllRows();	# Not memory efficient but may be useful 
 *
 *		UPDATE / DELETE
 *
 *		$sql=sprintf("DELETE FROM users WHERE userid = '%s''", $rs->MYSQLEscapeString($userid));
 *		$status = $rs->execute($sql);
 *
 *      </code>
*/
namespace Prompt\MySQL;

use Bot\Config\BotConfig;

class MYSQLRecordset {


	private $dbconnect;
	private $dbconnect_ro;
	private $params = array();
        private $realquery=false;
        


	/**
	* @access public
	* @var string
	*/
	public $result;
	

	/**
	*	Constructor
	*	@param  array	$hostname MYSQL Server hostname
	*	@param  array	$username MYSQL Server username
	*	@param  array	$password MYSQL Server password
	*	@param  array	$database MYSQL Server database
	*	@return	
	*/
	public function __construct($params=array()) {
                $params=array_merge(array(
                    'mysql_hostname'=>NULL,
                    'mysql_username'=>NULL, 
                    'mysql_password'=>NULL, 
                    'mysql_database'=>NULL,
                    'mysql_readonly_id'=>NULL
                ), $params);
                $this->params['mysql_hostname_ro']=NULL;
		$this->params['mysql_hostname'] = ($params['mysql_hostname']) ? $params['mysql_hostname'] : BotConfig::getValue("MYSQL_HOSTNAME");
		$this->params['mysql_username'] = ($params['mysql_username']) ? $params['mysql_username'] : BotConfig::getValue("MYSQL_USERNAME");
		$this->params['mysql_password'] = ($params['mysql_password']) ? $params['mysql_password'] : BotConfig::getValue("MYSQL_PASSWORD");
		$this->params['mysql_database'] = ($params['mysql_database']) ? $params['mysql_database'] : BotConfig::getValue("MYSQL_DATABASE");
                                
                $this->setConnection($this->params['mysql_hostname'], $this->params['mysql_username'], $this->params['mysql_password'], $this->params['mysql_database'],false);
                
                if($params['mysql_readonly_id']) {
                    $this->setReadOnlyID($params['mysql_readonly_id']);                    
                }
	}
        
        public function setConnection($mysql_hostname, $mysql_username, $mysql_password, $mysql_database, $ro=false) {
                if($ro) {
                    $this->dbconnect_ro = new MYSQLDBConnect($mysql_hostname, $mysql_username, $mysql_password, $mysql_database);              
                    $this->dbconnect_ro->connect();		
                } else {
                    $this->dbconnect = new MYSQLDBConnect($mysql_hostname, $mysql_username, $mysql_password, $mysql_database);              
                    $this->dbconnect->connect();		
                }
        }
        
        public function setReadOnlyID($id) {            
            $this->params['mysql_hostname_ro'] = \Prompt\Config\ServiceConfiguration::getValue(sprintf("MYSQL_HOSTNAME_READONLY_%s", $id));
            # If it's not set, fall back to master
            if(!$this->params['mysql_hostname_ro']) { $this->params['mysql_hostname_ro']=$this->params['mysql_hostname']; }
            $this->setConnection($this->params['mysql_hostname_ro'], $this->params['mysql_username'], $this->params['mysql_password'], $this->params['mysql_database'],true);
        }
	
	/**	
	*	Automatic shutdown function
	*	@param
	*	@return	
	*/
	public function __destruct() {
                foreach ($this as $index => $value) unset($this->$index);
		#$this->freeResult();
	}	

	/**	
	*	Frees MYSQL result set
	*	@param
	*	@return	
	*/
	public function freeResult() {
		if($this->result) {
			@$this->result->free(); //mysqli_result::free($this->result);
		}
	}
        
        public function query() {

            $l = func_num_args();
            $args = func_get_args();
            

            if($l>0) {
                $sql=$args[0];
                $action=strtoupper(substr($sql,0,6));
                
                $this->realquery=true; //in_array($action, array("SELECT"));

            
                if($action=="SELECT" && $this->dbconnect_ro) {
                    $target = & $this->dbconnect_ro;
                } else {
                    $target = & $this->dbconnect;
                }                        

                try {
                    $stmt = $target->getConnection()->prepare($sql); 
                    if($l>1) {
                        # Bind param only accepts variable references
                        $params = array();
                        $typestr = '';
                        for($i=1;$i<$l;$i++) { 
                            $params[] = & $args[$i]; 
                            $typestr.='s';
                        }
                        $bindparams = array_merge(array($typestr), $params);                      
                        call_user_func_array(array($stmt,'bind_param'), $bindparams); 
                    }
                    $stmt->execute();
                    //printf("rows inserted: %d\n", $stmt->affected_rows);
                    $this->result = $stmt->get_result();
                    //echo "<pre>";print_r($this->result);
		} catch (\Exception $e) {
                    error_log(sprintf('Caught exception: %s',  $e->getMessage()));
                    throw new \Exception($e);
                }                
            }            
        }

	/**	
	*	Run MYSQL query
	*	@param	string	MYSQL SQL statement
	*	@return	
	*/
	public function rawquery($sql,$params=array()) {
                $params=array_merge(array('realquery'=>false), $params);
                $action=strtoupper(substr($sql,0,6));
                if($action=="SELECT" && $this->dbconnect_ro) {
                    $target = & $this->dbconnect_ro;
                } else {
                    $target = & $this->dbconnect;
                }
		try {
                    if($params['realquery']) {
                        # http://dev.mysql.com/doc/apis-php/en/apis-php-mysqli.quickstart.statements.html
                        $this->realquery=true;
                        $target->getConnection()->real_query($sql) or trigger_error(sprintf("Unable to execute query: %s\n\n%s",$this->dbconnect->getConnection()->error,$sql));
                        $this->result=$target->getConnection()->use_result();
                    } else {
                        $this->realquery=false;
                        $this->result=$target->getConnection()->query($sql) or trigger_error(sprintf("Unable to execute query: %s\n\n%s",$this->dbconnect->getConnection()->error,$sql));
                    }
		} catch (\Exception $e) {
                    error_log(sprintf('Caught exception: %s',  $e->getMessage()));
                }
	}

	/**	
	*	Fetch next record from recordset
	*	@param
	*	@return	hash	MYSQL row as hash (if there is one)
	*/
	public function fetchNext() {
            return $this->result->fetch_assoc();
	}

	/**	
	*	Fetch the specified record from recordset
	*	@param	int		Row number to return
	*	@return	hash	MYSQL row as hash (if there is one)
	*/
	public function fetchRow($i=0) {
		$this->seek($i);
		return $this->fetchNext();
	}

	/**	
	*	Move the recordset pointer to the specified row
	*	@param	int		Row number
	*	@return	
	*/
	public function seek($i=0) {
		$this->result->data_seek($i);
	}
        
	/**	
	*	Return the number of affected rows from last statement
	*	@param
	*	@return	int	number of rows found
	*/
	public function getAffectedRows() {
		return $this->dbconnect->getConnection()->affected_rows;
	}        

	/**	
	*	Return the number of rows found from last statement
	*	@param
	*	@return	int	number of rows found
	*/
	public function getNumRows() {
		return ($this->realquery) ? $this->result->num_rows : $this->dbconnect->getConnection()->affected_rows;
	}

	/**	
	*	Alias for getNumRows()
	*	@param
	*	@return	int	number of rows found
	*/	
	public function getCount() {
		return $this->getNumRows();
	}

	/**	
	*	Fetch all records from recordset
	*	@param
	*	@return	hash	Array of MYSQL rows as hash (if there are any)
	*/
	public function fetchAllRows() {
		if($this->getNumRows()>0) {	$this->seek(0);	}
		//return $this->result->fetch_all(MYSQLI_ASSOC); Needs mysqlnd
		$arr=array();
		while($row=$this->fetchNext()) {
			array_push($arr,$row);
		}
		if($this->getNumRows()>0) {
			$this->seek(0);
		}
		return $arr;
	}
	
	/**
	*	Escapes a string using the connectors real_escape_string function
	*	@param  string	$str String to escape
	*	@return	string	Escaped string
	*/
	public function MYSQLEscapeString($str) {
		return $this->dbconnect->MYSQLEscapeString($str);
	}	

	/**	
	*	Execute a MYSQL statement
	*	@param	string	MYSQL SQL statement
	*	@return	hash	Status hash of executed statement
	*/
	public function execute($sql) {
		$retval=array('insertid'=>NULL,'affectedrows'=>NULL);
		
		try {
			$this->result=$this->rawquery($sql); 

			$action=strtoupper(substr($sql,0,6));
			if($action=="INSERT") {
				$this->serialid=(isset($this->dbconnect->getConnection()->insert_id)) ? $this->dbconnect->getConnection()->insert_id : NULL;
				$retval['insertid']=$this->serialid;
			}
			if($action=="UPDATE" || $action=="DELETE" || $action=="INSERT") {
				$retval['affectedrows']=$this->getAffectedRows(); 
			}
		} catch (\Exception $e) {
                    error_log(sprintf('Caught exception: %s',  $e->getMessage()));
                }
		return $retval;
	}


	/* Fetches enum values for specified table / column */
	public function getEnumValues($table,$column) {
		$cols=array();
		$sql=sprintf("SELECT COLUMN_TYPE 
                                FROM INFORMATION_SCHEMA.COLUMNS 
                                WHERE TABLE_SCHEMA = '%s' 
                                AND TABLE_NAME = '%s' 
                                AND COLUMN_NAME = '%s'",
			$this->MYSQLEscapeString(\Prompt\ServiceConfiguration::getValue("MYSQL_DATABASE")),
			$this->MYSQLEscapeString($table),
			$this->MYSQLEscapeString($column)
			);
		$this->query($sql);
		if($this->getNumRows()>0) {
			$row=$this->fetchNext();
			$row['COLUMN_TYPE']=preg_replace("/enum\('/","", $row['COLUMN_TYPE']);
			$row['COLUMN_TYPE']=preg_replace("/'\)$/","", $row['COLUMN_TYPE']);
			$cols=preg_split("/','/", $row['COLUMN_TYPE']);
		}
		return $cols;
	}	
 

}