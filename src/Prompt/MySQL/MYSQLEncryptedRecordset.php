<?php
/**
 *      MYSQLRecordset.class.php
*/
/**
 *		Provided and owned by Blue World Media as part of the bespoke framework
 *		utility functions. Provided under licence.
 *
 *      This class provides direct MYSQL Table access 
 *
 *
 *      Example:
 *      <code>
 *		$rs = new Prompt\MYSQLRecordset();
 *		$sql=sprintf("SELECT * FROM users WHERE lastupdated > '%s''", $rs->MYSQLEscapeString($lastupdated));
 *		$rs->query($sql);
 *		if($rs->getCount()>0) {
 *			while($row = $rs->fetchNext()) {
 *				echo sprintf("%s\n", $row['emai']);
 *			}
 *		}
 *	
 *		or
 *
 *		$sql=sprintf("SELECT * FROM users WHERE lastupdated > '%s''", $rs->MYSQLEscapeString($lastupdated));
 *		$rs->query($sql);
 *		$rows = $rs->fetchAllRows();	# Not memory efficient but may be useful 
 *
 *		UPDATE / DELETE
 *
 *		$sql=sprintf("DELETE FROM users WHERE userid = '%s''", $rs->MYSQLEscapeString($userid));
 *		$status = $rs->execute($sql);
 *
 *      </code>
*/
namespace Prompt\MySQL;


class MYSQLEncryptedRecordset extends MYSQLRecordset{


	/**
	*	Constructor
	*	@param  array	$hostname MYSQL Server hostname
	*	@param  array	$username MYSQL Server username
	*	@param  array	$password MYSQL Server password
	*	@param  array	$database MYSQL Server database
	*	@return	
	*/
	public function __construct($params=array()) {
                parent::__construct();
	}
        
	/**	
	*	Fetch next record from recordset
	*	@param
	*	@return	hash	MYSQL row as hash (if there is one)
	*/
	public function fetchNext() {
            return $this->result->fetch_assoc();
	}

	/**	
	*	Fetch all records from recordset
	*	@param
	*	@return	hash	Array of MYSQL rows as hash (if there are any)
	*/
	public function fetchAllRows() {
            # Calls $this->fetchNext()
            return parent::fetchAllRows();
	}
	

	/**	
	*	Execute a MYSQL statement
	*	@param	string	MYSQL SQL statement
	*	@return	hash	Status hash of executed statement
	*/
	public function execute($sql) {
            $action=strtoupper(substr($sql,0,6));
            if($action=="UPDATE" || $action=="DELETE" || $action=="INSERT") {

            }
            return parent::execute($sql);
	}

}
