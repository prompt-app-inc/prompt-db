<?php
/**
 *      MYSQLDBConnect.class.php
*/
/**
 *		Provided and owned by Blue World Media as part of the bespoke framework
 *		utility functions. Provided under licence.
 *
 *      This class provides MYSQL Database Connectivity
 *
 *      Example:
 *      <code>
 *      </code>
*/

namespace Prompt\MySQL;


#require_once("Core/MYSQLDBConnectionPool.class.php");

class MYSQLDBConnect {



	private $hostname = "localhost";
	private $database = NULL;
	private $username = "";
	private $password = "";
	private $connection;


	/**
	*	Constructor
	*	@param  string	$hostname MYSQL Server hostname
	*	@param  string	$username MYSQL Server username
	*	@param  string	$password MYSQL Server password
	*	@param  string	$database MYSQL Server database
	*	@return	
	*/
	public function __construct($hostname="",$username="",$password="",$database="") {
		if(!$this->hostname || $this->hostname != $hostname) { $this->hostname=$hostname; }
		if(!$this->username || $this->username != $username) { $this->username=$username; }
		if(!$this->password || $this->password != $password) { $this->password=$password; }
		if(!$this->database || $this->database != $database) { $this->database=$database; }
	}

	/**
	*	Escapes a string using the connectors real_escape_string function
	*	@param  string	$str String to escape
	*	@return	string	Escaped string
	*/
	public function MYSQLEscapeString($str) {
		return $this->connection->real_escape_string($str);
	}

	/**	
	*	Connects to the MYSQL Database
	*	@return	
	*/
	public function connect() {
            
            if(!$this->connection = MYSQLDBConnectionPool::getConnection($this->hostname, $this->database, $this->username, $this->password)) {
		try {
                        #options( MYSQLI_OPT_CONNECT_TIMEOUT, 5 );
			$this->connection = new \mysqli($this->hostname, $this->username, $this->password, $this->database);
			if ($this->connection->connect_error) {
				trigger_error($this->connection->connect_error,E_USER_ERROR);
			}			
			if(defined("MYSQL_CHARSET")) {
				$this->connection->query( sprintf("SET NAMES %s;", MYSQL_CHARSET));
                                $this->connection->set_charset(MYSQL_CHARSET);
			}
                        MYSQLDBConnectionPool::setConnection($this->hostname, $this->database, $this->username, $this->password, $this->connection);
    		} catch (CustomException $e) {print_r($e);}
            }
	}

	/**	
	*	Closes the connection to the MYSQL Database
	*	@return	
	*/
	public function close() {
		try {
			if(isset($this->connection)) {
    				#@$this->connection->close();
                                MYSQLDBConnectionPool::closeConnection($this->hostname, $this->database, $this->username, $this->password);
				unset($this->connection);
			}
		} catch (CustomException $e) {}
	}

	/**	
	*	Returns MYSQL Connection
	*	@return	object MYSQL Connection
	*/
	public function getConnection() {
		return $this->connection;
	}


	/**	
	*	Selects MYSQL Connection
	*	@return	
	*/
	public function selectDatabase($database) {
		try {
			if($database) {
				$this->database=$database;
				$this->connection->select_db($this->database);
			}
		} catch (CustomException $e) {}
	}

	/**	
	*	Automatic shutdown function
	*	@return	
	*/
	public function __destruct() {
             #$this->close(); 
	}

}
