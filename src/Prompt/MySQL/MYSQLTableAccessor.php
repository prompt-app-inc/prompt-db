<?php
/**
 *      MYSQLTableAccessor.class.php
*/

/**
 *		Provided and owned by Blue World Media as part of the bespoke framework
 *		utility functions. Provided under licence. 
 *
 *      This abstract class provides MYSQL Table Accessibility
 *
 *		When extended, you must define $table,$serialkey (Array) and (optionaly) $fields
 * 		A quick way to get the field list is to call getTableFieldNamesAsString();
 *
 *      Example:
 *      <code>
 *		$users = new Prompt\Users();
 *		$users->load(1234); # Loads user ID 1234
 *		$users->setEmail($users->MYSQLEscapeString("james@blueworldmedia.com")); # Sets email address (escaping the string to prevent injection)
 *		$result = $users->save(); # Reports success or failure
 *      </code>
*/
namespace Prompt\MySQL;


abstract class MYSQLTableAccessor {

	protected $table= "";
	protected $serialkey = array();
	protected $fields = array();
	protected $datarec;
	protected $originalrec;
	protected $rs;
	protected $lastsql;
	protected $rowsreturned=0;
        protected $lookupdata = array();
        protected $forceinsert = false;
        protected $permittedAjaxFields = array();
        protected $AJAXTypeDefArr = NULL;

	public function __construct($params=array()) {

                $params=array_merge(array(
                    'mysql_hostname'=>NULL,
                    'mysql_username'=>NULL, 
                    'mysql_password'=>NULL, 
                    'mysql_database'=>NULL,
                    'mysql_readonly_id'=>NULL,
                    'rs'=>NULL,
                ), $params);
                
		$this->rs = ($params['rs']) ? $params['rs'] : new MYSQLRecordset($params);
		if(!$this->fields || count($this->fields)==0) {
			$this->loadDefaultFields();
		}

	}

	public function __destruct() {
            if(isset($this->rs)) {
                $this->rs->__destruct();
            }
            foreach ($this as $index => $value) unset($this->$index);
        }
        
                
        public function getFields() {
            return $this->fields;
        }
        
        public function setForceInsert($b) {
            $this->forceinsert=$b;
        }
	
	/**
	*	Loads the default fields based on the table schema.
	*	Override field list by defining $fields array in inherited module
	*/
	
	public function loadDefaultFields() {
		foreach($this->getTableFieldNames() as $field) {
			$this->fields[]=$field;
		}
	}

	/**
	*	Initialises eeach field value to NULL
	*/	
	public function initialize($asnull=false) {
            if($asnull) {
                $this->datarec = NULL;
            } else {
                foreach($this->getTableFieldNames() as $field) {
                        $this->set($field,NULL);
                }
            } 
            $this->originalrec=NULL;
            $this->lastsql=NULL;
	}
	
	/**
	*	Escapes a string using the connectors real_escape_string function
	*	@param  string	$str String to escape
	*	@return	string	Escaped string
	*/
	public function MYSQLEscapeString($str) {
		return $this->rs->MYSQLEscapeString($str);
	}	

	/**
	*	Magic function __call for dynamic method accessors
	*	@param string $method Method called
	*	@param string $args Method arguments
	*	@return	various
	*/
	public function __call($method, $args) {
		$retval=NULL;
		try {
			$action=substr($method,0,3);
			$key=strtolower(substr($method,3));
			if(!in_array($key, $this->fields)) {
				$action="invalid";
			}
			switch($action) {
				case "get":
						$retval = $this->get($key);
						break;
				case "set":
						$this->set($key,$args[0]);
						break;
				default:
						//debug_print_backtrace();
						die(sprintf('Call to invalid method %s() in %s',$method, get_called_class()));
						#throw new CustomException(new ErrorException(sprintf("No such method %s", $method)));
						exit;
						break;		
			}
		} catch (CustomException $e) { }
		//var_dump($method, $args[0]);
		return $retval;
	}

	/**
	*	Generic get accessor
	*	@param string $key Dataset key
	*	@return	mixed Data value
	*/
	public function get($key) {
		return (isset($this->datarec[$key])) ? $this->datarec[$key] : NULL;
	}
	
	/**
	*	Generic set accessor
	*	@param string $key Dataset key
	*	@param various $args Data value
	*	@return	
	*/
	public function set($key,$value) {
		$this->datarec[$key]=$value;
	}


	/**
	*	Return field names for this table
	*	@param 
	*	@return	mixed Column information for this table
	*/
	public function getTableFieldNames() {
		$columns=array();
		try {
                    $this->rs->rawquery(sprintf("SHOW COLUMNS FROM %s", $this->MYSQLEscapeString($this->table)));
                    if($this->rs->getCount()>0) {
                            while($row=$this->rs->fetchNext()) {
                                    $columns[]=$row['Field'];
                            }
                    }
		} catch (CustomException $e) {}
		return $columns;
	}

	/**
	*	Return field names for this table as a string
	*	@param 
	*	@return	string Column information for this table as a string
	*/

	public function getTableFieldNamesAsString() {
		return sprintf("'%s'", join("','", $this->getTableFieldNames()));
	}
	
	
	/**
	*	Load record from MYSQL Table
	*	@param string $keyvalues Either a singleton or a hash to match the array keys (eg Array('name'=>'jon', 'id'=>5) )
	*	@param string SQL filter clause (optional)
	*	@return	mixed Record as hash
	*/	
	
	public function load($keyvalues, $filtersql="") {
		
		$sql=sprintf("SELECT * FROM %s WHERE 1=1", $this->MYSQLEscapeString($this->table));
		if(isset($keyvalues)) {
			#$sql.=" 1=1";
			if(is_array($this->serialkey) && count($this->serialkey)>1) {
				foreach($this->serialkey as $key) {
						$sql.=sprintf(" AND %s = '%s'", $key, $this->MYSQLEscapeString($keyvalues[$key]));
				}
			} else {
						$sql.=sprintf(" AND %s = '%s'", $this->serialkey[0], $this->MYSQLEscapeString($keyvalues));
			}
		} 
		if($filtersql!="") {
			$sql=sprintf("%s AND %s", $sql, $filtersql);
		}
		if($sql!=$this->lastsql || !$this->datarec) {
			$this->lastsql = $sql;
			try {
				$this->rs->query($sql);
				$this->rowsreturned = $this->rs->getCount();
				switch($this->rowsreturned) {
					case 0:
						$this->datarec=NULL;
						break;
					case 1:
						$this->datarec=$this->rs->fetchNext();
						break;
					default:
                                                error_log(sprintf('MON: MYSQLTableAccessor()->load() return more than one row - %s', $sql));
						$this->datarec=$this->rs->fetchAllRows();
						break;
				}
			} catch(CustomException $e) {};		
			$this->originalrec = $this->datarec;
		}
		return $this->datarec;
	}
        
	/**
	*	Return if a record is currently loaded 
        *      Does NOT mean the record has been taken from the DB and has a serial key
        *       it means the record has all its field initialised 
	*	@param 
	*	@return	bool	true or false
	*/
	public function isLoaded($params = array()) {
            $params = array_merge(array('withserial' => true), $params);            
            $hasData = (is_array($this->datarec) && count($this->datarec)>=count($this->fields));
            $hasSerial = ($params['withserial']) ? isset($this->datarec[$this->serialkey[0]]) && $this->datarec[$this->serialkey[0]] : true;
            return $hasData && $hasSerial;
	}

	/**
	*	Return the currently loaded record in memory
	*	@param 
	*	@return	mixed Current record as hash
	*/
	public function getRecord() {
		return $this->datarec;
	}

	/**
	*	Sets the data record in memory
	*	@param array $datarec	Dataset. Ensure the fields match the table definition before priming.
	*	@param boolean	$merge 	Merge the data rather than overwrite. true or false.
	*	@param boolean	$permittedFields  Array of field names that may be merged (for filtering).
	*	@return	
	*/
	public function prime($datarec, $merge=true, $permittedFields=array()) {
                $permitAll = is_null($permittedFields) || count($permittedFields)==0;
		foreach($this->fields as $field) {
                    if($permitAll || in_array($field, $permittedFields)) {
                        $val = (isset($datarec[$field])) ? $datarec[$field] : (($merge && isset($this->datarec[$field])) ? $this->datarec[$field] : NULL);
                        if(gettype($val)=='string') {
                            $lval = strtolower($val);
                            if($lval=='true' || $lval=='false') {
                                $val=($val=='false') ? false : true;
                            }
                        }
			$this->datarec[$field] = $val;
                    }
		}
	}
        
        public function primeField($field, & $src, $default=NULL) {
            #if(!$src) { $src= & $_REQUEST; }
            if(in_array($field,$this->fields) && isset($src[$field])) {
                $this->datarec[$field] = $src[$field];
            } else {
                if($default && (!isset($this->datarec[$field]) || !$this->datarec[$field])) {
                    $this->datarec[$field]=$default;
                }
            }
        }

	/**
	*	Sets the if specific field data in the dataset is different from the once supplied
	*	@param array $compareArr	Dataset to check against. Comparison is made via $fields against the master record.
	*	@param array $fields	Array of fields to check.
	*	@return boolean	True if same, otherwise false.
	*/
	public function haveFieldsChanged(& $compareArr, $fields=NULL) {
		$same=true;
		$source=(isset($fields)) ? $fields : array_keys($this->datarec);
		if(is_array($compareArr)) {
			foreach($fields as $key) {
				if(!isset($compareArr[$key]) || $this->datarec[$key]!=$compareArr[$key]) {
					$same=false;
					break;
				}
			}
		}
		return $same;						
	}
	
	public function hasChanged() {
		return $this->datarec!=$this->originalrec;
	}
        
	public function hasFieldChanged($field) {
		return $this->datarec[$field]!=$this->originalrec[$field];
	}        

	/**
	*	Save record to MYSQL Table
	*	@param 
	*	@return	mixed Status record
	*/	
	
	public function save() {
		$mode=NULL;
		$sql="";
		$wheresql="";
		$found=true;
		if(is_array($this->serialkey) && count($this->serialkey)>1) {
			foreach($this->serialkey as $key) {
				if(!isset($this->datarec[$key])) { 
					$found=false;
					#break;
					$wheresql.=sprintf(" AND %s = '%s'", $key, $this->MYSQLEscapeString($keyvalues[$key]));
				}
			}
		} else {
			$found=isset($this->datarec[$this->serialkey[0]]) && $this->datarec[$this->serialkey[0]]>0;	# CHANGED TO CHECK >0 11 APRIL 2014
			if($found) {
				$wheresql.=sprintf(" AND %s = '%s'", $this->serialkey[0], $this->datarec[$this->serialkey[0]]);
			}
		}
		$mode=(!$found || $this->forceinsert==true) ? "INSERT" : "UPDATE";
		$retcode=NULL;
		if($mode=="UPDATE") {			
			if($this->hasChanged()) {
				# Update
				$updateSQLArr=array();
				foreach($this->fields as $validfield) {
					if(array_key_exists($validfield, $this->datarec) && !is_null($this->datarec[$validfield])) {
						$updateSQLArr[]=sprintf("%s = %s", $validfield, 
													((string)$this->datarec[$validfield]!="NULL") ? sprintf("'%s'", $this->MYSQLEscapeString($this->datarec[$validfield])) : "NULL"
													);
					}
				}
				if(count($updateSQLArr)>0) {
					$sql=sprintf("UPDATE %s SET %s WHERE 1=1 %s", $this->table, join(", ",$updateSQLArr), $wheresql);
				}
			}
		} else {
			# Insert
			$colsArr=array();
			$valuesArr=array();
			foreach($this->fields as $validfield) {
				if(!in_array($validfield, $this->serialkey) || $this->forceinsert) {
					if(array_key_exists($validfield, $this->datarec) && !is_null($this->datarec[$validfield])) {
						$colsArr[] = $validfield;	
						$valuesArr[]=((string)$this->datarec[$validfield]!="NULL") ? sprintf("'%s'", $this->MYSQLEscapeString($this->datarec[$validfield])) : "NULL";
					}
				}
			}						
			if(count($colsArr)>0) {
				$sql=sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, join(", ",$colsArr), join(", ",$valuesArr));
			}
		}

                if($sql!="") {
                    
			$retcode=$this->rs->execute($sql);

			if($mode=="INSERT" && is_array($this->serialkey) && count($this->serialkey)==1) {
				$this->set($this->serialkey[0],$retcode['insertid']);
			}
                        
                        // Store the update against the original again
                        $this->originalrec = $this->datarec;
                        
		}
                
		return $retcode;
	}

	/**
	*	Return a count of all records for this type
	*	@param string $filter SQL filter clause eg. AND online = 'Y'
	*	@return	int	Count of  records
	*/		
	public function getCount($filter=NULL) {
		$sql=sprintf("SELECT COUNT(*) AS num FROM %s WHERE 1=1", $this->table);
		return $this->processGetRowCount($sql, $filter);
	}
	
	public function getRowsReturned() {
		return $this->rowsreturned;
	}

	/**
	*	Processes SQL generated by getCount() or can be given a custom SQL statement by child class
	*	@param string $sql SQL statement to process
	*	@param string $filter SQL filter clause eg. AND online = 'Y'
	*	@return	array Array of records
	*/	
	protected function processGetRowCount($sql, $filter=NULL) {
		if(!empty($filter)) {
			$sql.=sprintf(" AND %s", $filter);
		}
		$this->rs->query($sql);
		if($this->rs->getCount()>0) {
			$row = $this->rs->fetchNext();
			$rowcount = $row['num'];
		}
		
		return $rowcount;		
	}

	/**
	*	Return an array of all  records
	*	@param string $filter SQL filter clause eg. AND online = 'Y'
	*	@param string $orderby	Order by field list (as comma delimiter string)
	*	@param string $offset	Offset to start returning rows from
	*	@param string $limit	Limit to this number of rows only
	*	@return	array Array of records
	*/		
	public function getRows($filter=NULL, $orderby=NULL, $offset=NULL, $limit=NULL) {
		$sql=sprintf("SELECT * FROM %s WHERE 1=1", $this->table);
		return $this->processGetRows($sql, $filter, $orderby, $offset, $limit);
	}


	/**
	*	Processes SQL generated by getRows() or can be given a custom SQL statement by child class
	*	@param string $sql SQL statement to process
	*	@param string $filter SQL filter clause eg. AND online = 'Y'
	*	@param string $orderby	Order by field list (as comma delimiter string)
	*	@param string $offset	Offset to start returning rows from
	*	@param string $limit	Limit to this number of rows only
	*	@return	array Array of records
	*/	
	protected function processGetRows($sql, $filter=NULL, $orderby=NULL, $offset=NULL, $limit=NULL) {
		$rowArr=array();
		
		if(!empty($filter)) {
			$sql.=sprintf(" AND %s", $filter);
		}
		if(isset($orderby)) {
			$sql.=sprintf(" ORDER BY %s", $this->MYSQLEscapeString($orderby));		
		}
		if(isset($offset) && isset($limit)) {
			$sql.=sprintf(" LIMIT %s, %s", $this->MYSQLEscapeString($offset), $this->MYSQLEscapeString($limit));
		} else {
			if(isset($limit)) {
				$sql.=sprintf(" LIMIT %s", $this->MYSQLEscapeString($limit));
			}
		}
		$this->rs->query($sql);
		if($this->rs->getCount()>0) {
			$rowArr = $this->rs->fetchAllRows();
		}
		
		return $rowArr;		
	}

	/**
	*	Delete record from MYSQL Table
	*	@param mixed	$keyvalues	Key value or has of values
	*	@param string	$swheresql	Optional WHERE SQL statement
	*	@return	int	Number of rows deleted
	*/	
	public function delete($keyvalues=NULL, $wheresql=NULL) {
		$affectedrows=NULL;	
		try {	
		
                        $canexecute=false;
                        if(!$keyvalues && is_array($this->serialkey) && count($this->serialkey)==1) {
                            $keyvalues=$this->get($this->serialkey[0]);
                        }
                        
                    
			$sql=sprintf("DELETE FROM %s WHERE 1=1", $this->MYSQLEscapeString($this->table));

			if($keyvalues) {
				#$sql.=" 1=1";
				if(is_array($this->serialkey) && count($this->serialkey)>1) {
					foreach($this->serialkey as $key) {
							$sql.=sprintf(" AND %s = '%s'", $key, $this->MYSQLEscapeString($keyvalues[$key]));
					}
				} else {
							$sql.=sprintf(" AND %s = '%s'", $this->serialkey[0], $this->MYSQLEscapeString($keyvalues));
				}
                                $canexecute=true;
			} 
			if($wheresql) {
				$sql=sprintf("%s AND %s", $sql, $wheresql);
                                $canexecute=true;
			}
                        if($canexecute) {
                            $resultrec = $this->rs->execute($sql);
                            $affectedrows = $resultrec['affectedrows'];
                        }
			
		} catch (CustomException $e) {}
		return $affectedrows;
	}

	/**
	*	Return the serial key for this record
	*	@param 
	*	@return	array	Serial key
	*/	

	public function getSerialKey() {
		return (is_array($this->serialkey) && count($this->serialkey)>1) ? $this->serialkey : $this->serialkey[0];
	}

        public function getSerialID() {
            return $this->get($this->getSerialKey());
        }
        
        public function getDataRecord() {
            return $this->datarec;
        }
        
        public function getEnumAsKeyValuePair($column, $params=array()) {
            $params=array_merge(array('key'=>'key','value'=>'value'), $params);
            $retArr=array();
            foreach($this->getEnumValues($column) as $value) {
                $retArr[] = array($params['key'] => $value, $params['value'] => $value);
            }
            return $retArr;
        }
        
        public function getEnumValues($column) {
            return $this->rs->getEnumValues($this->table,$column);
        }
        
        public function isDataCodeValid($column,$code) {
            return (isset($this->lookupdata[$column][$code]));
        }        
        
        public function getDataValue($column,$code) {
            #return (isset($this->lookupdata[$column][$code])) ? $this->lookupdata[$column][$code] : $code;
            $v = (isset($this->lookupdata[$column][$code])) ? $this->lookupdata[$column][$code] : NULL;
            if(!$v) {
                $v = $this->loadDataValue($column,$code,false);
            }
            return ($v) ? $v : $code;

        }
        
        public function setDataValueArray($column, $arr) {
            return $this->lookupdata[$column] = $arr;
        }         

        public function getDataValueArray($column) {
            return (isset($this->lookupdata[$column])) ? $this->lookupdata[$column] : array();
        }        
        
        public function loadDataValues($column) {         
            $sld = new \Prompt\SysLookupData();            
            foreach($sld->getSysLookupData(array('dbtable'=>$this->table,'dbcolumn'=>$column)) as $row) {
                $this->lookupdata[$column][$row['lookupcode']]=$row['lookupvalue'];
            }            
        }
        
        public function loadDataValue($column,$lookupcode, $returnid=false) {
            $sld = new \Prompt\SysLookupData();
            $value = $sld->loadDataValue($this->table,$column,$lookupcode,$returnid);
            return $value;
        }
        
        
        public function validateDataValue($column,$value) {
            $sld = new \Prompt\SysLookupData();
            return $sld->validateDataValue($this->table,$column,$value);            
        }
        
        public function validateRecord($data, $params=array()) {
            $params=array_merge(array('validateblank'=>false,'blanktonull'=>false), $params);
            $result = array('data' => $data,'validation'=>array());
            # Check sysdata look-ups
            if(isset($this->syslookupfields) && is_array($this->syslookupfields)) {
                foreach($this->syslookupfields as $field) {
                    if(isset($data[$field])) {
                        if((!$data[$field] && $params['validateblank']) || 
                           ($data[$field] && !$this->validateDataValue($field, $data[$field]))
                           ) {
                            $result['data'][$field]=NULL;
                            $result['validation'][$field]=sprintf("%s is not a valid value", $data[$field]);
                        }
                        if($params['blanktonull'] && !$data[$field]) {
                            $result['data'][$field]='NULL';
                        }
                    }
                }
            }
            # Check class level values
            if(isset($this->lookupdata) && is_array($this->lookupdata)) {
                foreach($this->lookupdata as $field) {
                    if(isset($data[$field])) {
                        if(!$this->isDataCodeValid($field, $data[$field])) {
                            $result['data'][$field]=NULL;
                            $result['validation'][$field]=sprintf("%s is not a valid value", $data[$field]);
                        }
                    }                            
                }            
            }            
            
            return $result;
        }        
        
        public function insertDataValue($column,$lookupcode, $lookupvalue, $orderby=NULL) {
            $sld = new \Prompt\SysLookupData();
            return $sld->insertDataValue($column,$lookupcode, $lookupvalue, $orderby);             
        }
        
        public function sanitizeForAJAXCallback($key, $value) {
            return $value;
        }        

        public function sanitizeArrayForAJAX($mode, $data, $callback=NULL) {
            if(is_array($data)) {
                foreach($data as $key=>$value) {
                    $data[$key] = $this->sanitizeForAJAX($mode, $value);
                }
            }
            return $data;
        }          
        
        public function getPermittedAJAXFields($mode) {
            return (isset($this->permittedAjaxFields[$mode])) ? $this->permittedAjaxFields[$mode] : $this->permittedAjaxFields['public'];
        }

        public function sanitizeForAJAX($mode, $data) {

            // Assign typedefs
            if(!$this->AJAXTypeDefArr) {
                foreach($this->permittedAjaxFields as $type=>$fields) {
                    $this->AJAXTypeDefArr[$type]=array_keys($fields);                       
                }
            }
            
            $permittedAjaxFields = $this->getPermittedAJAXFields($mode);
            
            if(is_array($data)) {                
                foreach($data as $key=>& $value) {
                    if(!in_array($key, $permittedAjaxFields, true)) {
                        if(!in_array($key, $this->AJAXTypeDefArr[$mode], true)) {
                            unset($data[$key]);
                        } else {
                            if(isset($permittedAjaxFields[$key]['type'])) {
                                switch($permittedAjaxFields[$key]['type']) {
                                    case "integer":
                                        $value=(int)$value;
                                        break;
                                    case "float":
                                        $value=(float)$value;
                                        break;
                                    case "string":
                                        $value=(string)$value;
                                        break;
                                    case "boolean":
                                        $lcvalue=(!is_numeric($value)) ? strtolower($value) : $value;
                                        $value=($lcvalue=="yes" || $lcvalue=="true" || $lcvalue!=0);
                                        break;
                                }
                            }
                        }
                    } 
                                           #  echo $key."<br>";

                    #if(isset($data[$key])) { # Wont include NULL's
                    if(key_exists($key, $data)) { # Wont include NULL's
                        $value = $this->sanitizeForAJAXCallback($key,$value); # Returns to a reference
                    }
                }
            }

            return $data;
        }               
        
}