<?php

/**
 *      MYSQLTableAccessor.class.php
 */
/**
 * 		Provided and owned by Blue World Media as part of the bespoke framework
 * 		utility functions. Provided under licence. 
 *
 *      This abstract class provides MYSQL Table Accessibility
 *
 * 		When extended, you must define $table,$serialkey (Array) and (optionaly) $fields
 * 		A quick way to get the field list is to call getTableFieldNamesAsString();
 *
 *      Example:
 *      <code>
 * 		$users = new Prompt\Users();
 * 		$users->load(1234); # Loads user ID 1234
 * 		$users->setEmail($users->MYSQLEscapeString("james@blueworldmedia.com")); # Sets email address (escaping the string to prevent injection)
 * 		$result = $users->save(); # Reports success or failure
 *      </code>
 */

namespace Prompt\MySQL;

abstract class MYSQLEncryptedTableAccessor extends MYSQLTableAccessor {

    public function __construct($params = array()) {
        $params = array_merge(array(
            'mysql_hostname' => NULL,
            'mysql_username' => NULL,
            'mysql_password' => NULL,
            'mysql_database' => NULL,
            'mysql_readonly_id' => NULL,
            'rs' => NULL,
                ), $params);

        parent::__construct($params);
        $this->rs = ($params['rs']) ? $params['rs'] : new MYSQLEncryptedRecordset($params);
    }

    public function __destruct() {
        parent::__destruct();
    }

    public function load($keyvalues, $wheresql = "") {
        $result = parent::load($keyvalues, $wheresql);
        return $result;
    }

    public function save() {
        return parent::save();
    }

    public function getRows($filter = NULL, $orderby = NULL, $offset = NULL, $limit = NULL) {
        $result = parent::getRows($filter, $orderby, $offset, $limit);
        return $result;
    }

}