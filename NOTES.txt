http://stackoverflow.com/questions/18160115/composer-create-project-from-private-repo

http://www.sitepoint.com/quick-tip-composer-github-develop-packages-interactively/

Now you can keep working on your local package and push your changes to Github, 
If you made some changes to the local package and run composer update before pushing to Github you will lose them.
Branches are used for versioning your package. 
Version names can either be numbered like 1.0, 1.0.*, etc,
or you’ll have to prefix your branch name with dev-*, 
in our example the dev-master version will look for the master branch. 
You can check the documentation for more info.